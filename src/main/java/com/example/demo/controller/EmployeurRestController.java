package com.example.demo.controller;

import com.example.demo.dao.EmployeurRepository;
import com.example.demo.model.Employeur;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/employeur")
public class EmployeurRestController {
    @Autowired
    private EmployeurRepository employeurRepository;
    @Autowired
    private AccountService accountService;
    @PostMapping("/register")
    Employeur SaveEmployeur(@RequestBody
                              Employeur employeur){
        return this.accountService.saveEmployeur(employeur.getCin(), employeur.getNom(), employeur.getPrenom(), employeur.getVille(), employeur.getTelephone(), employeur.getEmail(), employeur.getConfirmedPassword(), employeur.getUsername(), employeur.getPassword(), employeur.getPost(), employeur.getSalaire());
    }
    @GetMapping("/all")
    List<Employeur> AfficherTout(){
        return (List<Employeur>) this.employeurRepository.findAll();
    }
    @PutMapping("/update/{id}")
    Employeur updateEmployeur(@RequestBody Employeur employeur, @PathVariable String id){

             employeur.setId(id);
          return   this.accountService.saveEmployeur(employeur.getCin(), employeur.getNom(), employeur.getPrenom(), employeur.getVille(), employeur.getTelephone(), employeur.getEmail(), employeur.getConfirmedPassword(), employeur.getUsername(), employeur.getPassword(), employeur.getPost(), employeur.getSalaire());

    }
    @DeleteMapping("/delete/{id}")
    void deleteEmployeur(@PathVariable String id){
        try{
            this.employeurRepository.deleteById(id);
            System.out.println("c'est Employeur a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("/one/{id}")
    public Employeur getOne(@PathVariable String id){
        return employeurRepository.find_id(id);
    }

}

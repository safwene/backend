package com.example.demo.controller;

import com.example.demo.dao.CategorieRepository;
import com.example.demo.dao.ProduitRepository;
import com.example.demo.dao.SousCategorieRepository;
import com.example.demo.model.Produit;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin("**")
@RestController
@RequestMapping("/produit")
public class ProduitRestController {
    @Autowired
    private ProduitRepository produitRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;


@PostMapping("/register/{id}")
    Produit SaveProduit(@RequestBody Produit produit, @PathVariable String id) {//, @PathVariable String idC
    try {
       produit.setSousCategorie(sousCategorieRepository.find_id(id));
        System.out.println("categorie");
        return this.produitRepository.save(produit);

    } catch (Exception e) {
        System.out.println("catch");
        return null;
    }
}
@GetMapping("/all")
    List<Produit> AfficherTout(){
        return this.produitRepository.findAll();
    }
@PutMapping("/update/{id}")
    Produit updateProduit( @RequestBody Produit produit,@PathVariable String id){

    try {
        produit.setId(id);
        return this.produitRepository.save(produit);
    } catch (Exception e) {
        return null;
    }

}
@DeleteMapping("/delete/{id}")
    void deleteProduit(@PathVariable String id){
    try{
        this.produitRepository.deleteById(id);
        System.out.println("c'est produit a été éffacer");
    }
    catch (Exception e){
        System.out.println(e.getMessage());
    }
}
@GetMapping("/one/{id}")
 public Produit getOne(@PathVariable String id){
    return produitRepository.find_id(id);
}

}

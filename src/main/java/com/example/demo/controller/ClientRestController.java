package com.example.demo.controller;

import com.example.demo.dao.ClientRepository;
import com.example.demo.model.Client;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/client")
public class ClientRestController {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AccountService accountService;
    @PostMapping("/register")
    Client SaveClient(@RequestBody
                              Client  client){
        return this.accountService.saveClient(client.getCin(), client.getNom(), client.getPrenom(), client.getVille(), client.getTelephone(), client.getEmail(), client.getConfirmedPassword(), client.getUsername(), client.getPassword(), client.getAdresse());
    }
    @GetMapping("/all")
    List<Client> AfficherTout(){
        return (List<Client>) this.clientRepository.findAll();
    }
    @PutMapping("/update/{id}")
    Client updateClient(@RequestBody Client client, @PathVariable String id){
        try {
            client.setId(id);
            return this.clientRepository.save(client);
        }
        catch (Exception e){
            return null;
        }

    }
    @DeleteMapping("/delete/{id}")
    void deleteClient(@PathVariable String id){
        try{
            this.clientRepository.deleteById(id);
            System.out.println("c'est client a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("/one/{id}")
    public Client getOne(@PathVariable String id){
        return clientRepository.find_id(id);
    }

}

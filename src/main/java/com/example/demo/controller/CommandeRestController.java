package com.example.demo.controller;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.CommandeRepository;
import com.example.demo.dao.ProduitRepository;
import com.example.demo.model.Client;
import com.example.demo.model.Commande;
import com.example.demo.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/commande")
public class CommandeRestController {
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private ProduitRepository produitRepository;
    @Autowired
    private ClientRepository clientRepository;

    private List<Commande> list;
    @PostMapping("/register/{idC}")
    Commande ajouterCommande(@RequestBody Commande commande, @RequestBody Produit produit, @PathVariable String idC){
        commande.getProduitList().add(this.produitRepository.find_id(produit.getId()));
        commande.setClient(this.clientRepository.find_id(idC));
        return commandeRepository.save(commande);
    }
    @GetMapping("/all")
    List<Commande> Tout(){
        return this.commandeRepository.findAll();
    }
    @GetMapping("/allClientCommande/{idC}")
    List<Commande> AfficherTout(@PathVariable String idC){

        List<Commande> listes = this.commandeRepository.findAll();
                listes.forEach(r->{
            if (r.getClient()== this.clientRepository.find_id(idC)){this.list.add(this.commandeRepository.find_id(r.getId()));}

        });
        return this.list;
    }

    @PutMapping("/ajouterProduit/{id}")
    Commande AjouterProduit(@RequestBody Commande commande,@RequestBody Produit produit, @PathVariable String id){
        commande.getProduitList().add(produit);
        commande.setId(id);
        return this.commandeRepository.save(commande);
    }
    @PutMapping("/supprimerProduit/{id}/{idP}")
    Commande SupprimerProduit(@RequestBody Commande commande, @PathVariable String id, @PathVariable String idP){
        commande.getProduitList().forEach(r->{
           if (r.getId()== idP){r.setId(null);}

        });
        commande.setId(id);
        return this.commandeRepository.save(commande);
    }
    @DeleteMapping("/delete/{id}")

    void deleteProduit(@PathVariable String id){
        try{
            this.commandeRepository.deleteById(id);
            System.out.println("cette Commande a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}

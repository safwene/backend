package com.example.demo.controller;

import com.example.demo.dao.AdminRepository;
import com.example.demo.model.Admin;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class    AdminController  {
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private AccountService accountService;
    @PostMapping("/register")
    Admin SaveAdmin(@RequestBody
                              Admin admin){
        return this.accountService.saveAdmin(admin.getCin(), admin.getNom(), admin.getPrenom(), admin.getVille(), admin.getTelephone(), admin.getEmail(), admin.getConfirmedPassword(), admin.getUsername(), admin.getPassword());
    }
    @GetMapping("/all")
    List<Admin> AfficherTout(){
        return (List<Admin>) this.adminRepository.findAll();
    }
    @PutMapping("/update/{id}")
    Admin updateAdmin(@RequestBody Admin admin, @PathVariable String id){
        try {
            admin.setId(id);
            return this.adminRepository.save(admin);
        } catch (Exception e) {
            return null;
        }
    }
    @DeleteMapping("/delete/{id}")
    void deleteAdmin(@PathVariable String id){
        try{
            this.adminRepository.deleteById(id);
            System.out.println("c'est admin a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("/one/{id}")
    public Admin getOne(@PathVariable String id){
        return adminRepository.find_id(id);
    }

}

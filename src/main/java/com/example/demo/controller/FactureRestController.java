package com.example.demo.controller;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.FactureRepository;
import com.example.demo.dao.ProduitRepository;
import com.example.demo.model.Facture;
import com.example.demo.model.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("facture")
public class FactureRestController {
    @Autowired
    private FactureRepository factureRepository;
    @Autowired
    private ProduitRepository produitRepository;
    @Autowired
    private ClientRepository clientRepository;
    @PostMapping("/register")
    Facture SaveFacture(@RequestBody Facture facture, @RequestBody Produit produit, @PathVariable String idC){
        facture.setClient(clientRepository.find_id(idC));
        facture.getProduitList().add(produit);
        return this.factureRepository.save(facture);
    }
    @GetMapping("/all")
    List<Facture> AfficherTout(){
        return this.factureRepository.findAll();
    }
    @PutMapping("/update/{id}/")
    Facture updatefacture(@RequestBody Facture facture, @PathVariable String id){
        try {
            facture.setId(id);
            return this.factureRepository.save(facture);
        }
        catch (Exception e){
            return null;
        }
    }
    @DeleteMapping("/delete/{id}")
    void deleteFacture(@PathVariable String id){
        try{
            this.factureRepository.deleteById(id);
            System.out.println("c'est Facture a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("/one/{id}")
    public Facture getOne(@PathVariable String id){
        return factureRepository.find_id(id);
    }

}

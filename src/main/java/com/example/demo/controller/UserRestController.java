package com.example.demo.controller;

import com.example.demo.dao.UserRepository;
import com.example.demo.model.AppUser;
import com.example.demo.model.Response;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/user")
public class UserRestController {
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private AccountService accountService;


  @GetMapping("/user")
  public AppUser getProfil(Principal principal){
      AppUser user=userRepository.findByUsername(principal.getName());
      return  user;
  }

  @GetMapping("/all")
  public List<AppUser> getAll(){
    return userRepository.findAll();
  }
  /*@PostMapping("/add")
  public AppUser addUser(@RequestBody AppUser u){
    return this.userRepository.save(u);
  }*/
  @PutMapping("/update/{id}")
  public AppUser updateUser(@RequestBody AppUser user, @PathVariable  String id){
    user.setId(id);
    return userRepository.save(user);

  }
  @DeleteMapping("/delete/{id}")
public Response deleteUser(@PathVariable String id){

      Response res = new Response();
    System.out.println("id=" +id);
    try {
      userRepository.deleteById(id);
      res.setState("ok");
    }catch (Exception e){
      System.out.println(e.getMessage());
      res.setState("non");
    }
    return res ;
}
@GetMapping("/one/{id}")
  public AppUser getOne(@PathVariable  String id){
    return userRepository.find_id(id);
}
//@PostMapping("/login")
//  public HashMap<String,User> login(@RequestBody User user){
//  HashMap<String,User> hashMap = new HashMap<>();
//  try {
//    hashMap.put("data",userRepository.login(user.getLogin(),user.getMotDePasse()));
//  }catch (Exception e){
//hashMap.put("data",null);  }
//  return hashMap;

}




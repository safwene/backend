package com.example.demo.controller;

import com.example.demo.dao.CategorieRepository;
import com.example.demo.dao.EmployeurRepository;
import com.example.demo.dao.SousCategorieRepository;
import com.example.demo.model.Categorie;
import com.example.demo.model.SousCategorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategorieRestController  {
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @PostMapping("/register")
    Categorie SaveCategorie(@RequestBody
                            Categorie categorie){
        return this.categorieRepository.save(categorie);
    }

    @GetMapping("/all")
    List<Categorie> AfficherTout(){
        return this.categorieRepository.findAll();
    }

    @PutMapping("/update/{id}")
    Categorie updateCategorie(@RequestBody Categorie categorie, @PathVariable String id){
        try {
            categorie.setId(id);
            return this.categorieRepository.save(categorie);
        } catch (Exception e) {
            return null;
        }
    }
    @DeleteMapping("/delete/{id}")
    void deleteCategorie(@PathVariable String id){
        try{
            this.categorieRepository.deleteById(id);
            System.out.println("cette Categorie a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
  /*  @GetMapping("/one/{id}")
    public Categorie getOne(@PathVariable String id){
        return categorieRepository.find_id(id);
    }
*/
}

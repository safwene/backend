package com.example.demo.controller;

import com.example.demo.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import java.io.File;

@RestController
@RequestMapping("/v1/email")
public class EmailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @RequestMapping("version")
    @ResponseStatus(HttpStatus.OK)
    public String version() {
        return "[OK] Welcome to withdraw Restful version 1.0";
    }

    @RequestMapping(value = "send", method = RequestMethod.POST, produces = { "application/xml", "application/json" })
    public ResponseEntity<Email> sendSimpleMail(@RequestBody Email email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(email.getFrom());
        message.setTo(email.getTo());
        message.setSubject(email.getSubject());
        message.setText(email.getText());
        javaMailSender.send(message);
        email.setStatus(true);

        return new ResponseEntity<Email>(email, HttpStatus.OK);
    }

    @RequestMapping(value = "attachments", method = RequestMethod.POST, produces = { "application/xml", "application/json" })
    public ResponseEntity<Email> attachments(@RequestBody Email email) throws Exception {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(email.getFrom());
        mimeMessageHelper.setTo(email.getTo());
        mimeMessageHelper.setSubject(email.getSubject());
        mimeMessageHelper.setText("<html><body><img src=\"cid:banner\" >" + email.getText() + "</body></html>", true);

        FileSystemResource file = new FileSystemResource(new File("banner.jpg"));
        mimeMessageHelper.addInline("banner", file);

        FileSystemResource fileSystemResource = new FileSystemResource(new File("Attachment.jpg"));
        mimeMessageHelper.addAttachment("Attachment.jpg", fileSystemResource);

        javaMailSender.send(mimeMessage);
        email.setStatus(true);

        return new ResponseEntity<Email>(email, HttpStatus.OK);
    }

     // application.properties spring.mail.host
//    @RequestMapping(value = "sendmail", method = RequestMethod.POST, produces = { "application/xml", "application/json" })
//    public ResponseEntity<Email> sendmail(@RequestBody Email email) {
//        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
//        javaMailSender.setHost(email.getHost());
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom(email.getFrom());
//        message.setTo(email.getTo());
//        message.setSubject(email.getSubject());
//        message.setText(email.getText());
//        try{
//            javaMailSender.send(message);
//            email.setStatus(true);
//        }catch(Exception e){
//            email.setText(e.getMessage());
//            email.setStatus(false);
//        }
//
//        return new ResponseEntity<Email>(email, HttpStatus.OK);
//    }
}

package com.example.demo.controller;

import com.example.demo.dao.CategorieRepository;
import com.example.demo.dao.SousCategorieRepository;
import com.example.demo.model.SousCategorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("**")
@RestController
@RequestMapping("/sousCategorie")
public class SousCategorieController {
    @Autowired
    private SousCategorieRepository sousCategorieRepository;
    @Autowired
    private CategorieRepository categorieRepository;


    @GetMapping("/getall/{id}")
    public List<SousCategorie> get(@PathVariable String id){
        return sousCategorieRepository.find_categorie(id);
    }
    @PostMapping("/register/{id}")
    SousCategorie SaveSousCategorie(@RequestBody SousCategorie sousCategorie, @PathVariable String id) {//, @PathVariable String idC
        try {
            sousCategorie.setCategorie(categorieRepository.find_id(id));
            return sousCategorieRepository.save(sousCategorie);

        } catch (Exception e) {
            System.out.println("catch");
            return null;
        }
    }
    @GetMapping("/all")
    List<SousCategorie> AfficherTout(){
        return this.sousCategorieRepository.findAll();
    }
    @PutMapping("/update/{id}")
    SousCategorie updateSousCategorie( @RequestBody SousCategorie sousCategorie,@PathVariable String id){

        try {
            sousCategorie.setId(id);
            return this.sousCategorieRepository.save(sousCategorie);
        } catch (Exception e) {
            return null;
        }

    }
    @DeleteMapping("/delete/{id}")
    void deleteProduit(@PathVariable String id){
        try{
            this.sousCategorieRepository.deleteById(id);
            System.out.println("cette Sous Categorie a été éffacer");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    @GetMapping("/one/{id}")
    public SousCategorie getOne(@PathVariable String id){
        return sousCategorieRepository.find_id(id);
    }

}

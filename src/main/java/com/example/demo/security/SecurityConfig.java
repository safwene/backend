package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/login/**","/user/register/**","/upload/**","/image/**").permitAll();//tous permet creer login et register
       // http.authorizeRequests().antMatchers("/produit/**","/client/**").permitAll();//tous permet creer login et register
        http.authorizeRequests().antMatchers("/appUsers/**","/appRoles/**").hasAuthority("ADMIN");//
      //  http.authorizeRequests().antMatchers("/appUsers/**","/appRoles/**").hasAuthority("USER");//
        //http.authorizeRequests().antMatchers("/user/**").hasAuthority("USER");//
        http.authorizeRequests().antMatchers("/admin/**").hasAuthority("ADMIN");//
       /// http.authorizeRequests().antMatchers("/admin/**").hasAuthority("EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/categories/all").permitAll();//
        http.authorizeRequests().antMatchers("/categories/register/**","/categories/update/**","/categories/delete/**","/categories/one/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/sousCategorie/register/**","/sousCategorie/all","/sousCategorie/update/**","/sousCategorie/delete/**","/sousCategorie/one/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/sousCategorie/getall/**").permitAll();//
        //http.authorizeRequests().antMatchers("/categories/**").hasAuthority("EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/client/**").permitAll();//
        http.authorizeRequests().antMatchers("/commande/**").permitAll();//
        http.authorizeRequests().antMatchers("/send/**").permitAll();//
        http.authorizeRequests().antMatchers("/commande/all/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/client/update/**").hasAuthority("CLIENT");//
        http.authorizeRequests().antMatchers("/client/all/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/employeur/**").hasAuthority("ADMIN");//
       // http.authorizeRequests().antMatchers("/employeur/update/**").hasAuthority("EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/facture/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/produit/register/**","/produit/update/**","/produit/delete/**","/produit/one/**").hasAnyAuthority("ADMIN,EMPLOYEUR");//
        http.authorizeRequests().antMatchers("/produit/all").permitAll();//

        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(new JWTAuthenticationFilter(authenticationManager()));
        http.addFilterBefore(new JWTAuthorizationFiler(), UsernamePasswordAuthenticationFilter.class);
    }
}
//li y5allini net7akem fi droit d'accées , generer l'autorité selon les roles

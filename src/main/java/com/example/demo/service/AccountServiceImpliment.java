package com.example.demo.service;

import com.example.demo.dao.RoleRepository;
import com.example.demo.dao.UserRepository;

import com.example.demo.model.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AccountServiceImpliment implements AccountService{
  private UserRepository userRepository;
  private RoleRepository roleRepository;
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  public AccountServiceImpliment(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }



    @Override
    public Client saveClient( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password, String adresse) {
        AppUser user = userRepository.findByUsername(username);
        if (user != null) throw new RuntimeException("User already exists");
        if (!password.equals(confirmedPassword)) throw new RuntimeException("Please confirm your password");
        Client appUser = new Client();
        appUser.setCin(cin);
        appUser.setNom(nom);
        appUser.setPrenom(prenom);
        appUser.setVille(ville);
        appUser.setTelephone(telephone);
        appUser.setEmail(email);
        appUser.setUsername(username);
        appUser.setAdresse(adresse);
        appUser.setPassword(bCryptPasswordEncoder.encode(password));
        appUser.setConfirmedPassword(bCryptPasswordEncoder.encode(confirmedPassword));

        Role role = roleRepository.findByRoleName("CLIENT");

        if (role != null) {
            appUser.getRoleList().add(role);
            userRepository.save(appUser);

        }
         addRoleToUser(username,"CLIENT");
        return appUser;
    }

    @Override
    public Employeur saveEmployeur( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password, String post, String salaire) {
        AppUser user = userRepository.findByUsername(username);
        if (user != null) throw new RuntimeException("User already exists");
        if (!password.equals(confirmedPassword)) throw new RuntimeException("Please confirm your password");
        Employeur appUser = new Employeur();
        appUser.setCin(cin);
        appUser.setNom(nom);
        appUser.setPrenom(prenom);
        appUser.setVille(ville);
        appUser.setTelephone(telephone);
        appUser.setEmail(email);
        appUser.setUsername(username);
        appUser.setPost(post);
        appUser.setSalaire(salaire);
        appUser.setPassword(bCryptPasswordEncoder.encode(password));
        appUser.setConfirmedPassword(bCryptPasswordEncoder.encode(confirmedPassword));

        Role role = roleRepository.findByRoleName("EMPLOYEUR");

        if (role != null) {
            appUser.getRoleList().add(role);
            userRepository.save(appUser);

        }
        addRoleToUser(username,"EMPLOYEUR");
        return appUser;
    }

    @Override
    public Admin saveAdmin( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password) {
        AppUser user = userRepository.findByUsername(username);
        if (user != null) throw new RuntimeException("User already exists");
        if (!password.equals(confirmedPassword)) throw new RuntimeException("Please confirm your password");
        Admin appUser = new Admin();
        appUser.setCin(cin);
        appUser.setNom(nom);
        appUser.setPrenom(prenom);
        appUser.setVille(ville);
        appUser.setTelephone(telephone);
        appUser.setEmail(email);
        appUser.setUsername(username);
        appUser.setPassword(bCryptPasswordEncoder.encode(password));
        appUser.setConfirmedPassword(bCryptPasswordEncoder.encode(confirmedPassword));

        Role role = roleRepository.findByRoleName("ADMIN");

        if (role != null) {
            appUser.getRoleList().add(role);
            userRepository.save(appUser);

        }
        addRoleToUser(username,"ADMIN");
        return appUser;
    }

    @Override
  //ajouter une role
  public Role save(Role role) {
    return roleRepository.save(role);
  }

  @Override
  //recherche de user
  public AppUser loadUserByUsername(String username)
  {
    return userRepository.findByUsername(username);
  }



  @Override
  //affectation de role d'utilisateur
  public void addRoleToUser(String username, String rolename) {

      AppUser appUser=userRepository.findByUsername(username);
    Role appRole=roleRepository.findByRoleName(rolename);

    appUser.getRoleList().add(appRole);
    //userRepository.save(appUser);
  }



  }


package com.example.demo.service;

import com.example.demo.model.*;

import java.util.List;


public interface AccountService {
  Client saveClient ( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password, String adresse);
  Employeur saveEmployeur( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password, String post, String salaire);
  Admin saveAdmin( int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username, String password);

  Role save(Role role);
  AppUser loadUserByUsername(String username);
  void addRoleToUser(String username, String rolename);
}

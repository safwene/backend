package com.example.demo;

import com.example.demo.dao.RoleRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.model.AppUser;
import com.example.demo.model.Role;
import com.example.demo.service.AccountService;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Resource
    StorageService storageService;
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
//initialliser lel foncion bech nesta3melha
    BCryptPasswordEncoder getBCPE() {
        return new BCryptPasswordEncoder();
    }


    public void run(String... args) throws Exception {
     //storageService.deleteAll();
       // storageService.init();
    }


//  @Bean
//    CommandLineRunner start(AccountService accountService) {
//        return args -> {
//
//            accountService.save(new Role("CLIENT"));
//            accountService.save(new Role("EMPLOYEUR"));
//            accountService.save(new Role("ADMIN"));
//
//
//            Stream.of("client1").forEach(un -> {
//           accountService.saveClient(1234,"mmm","kkkk","sousse",28467183,"saf@gmail.com","1234567","client1","1234567","mahdia");
//           });
//
//          AppUser user = userRepository.findByUsername("client1");
//            Role role = roleRepository.findByRoleName("CLIENT");
//           List<Role> roles = new ArrayList<>();
//          roles.add(role);
//           user.setRoleList(roles);
//           userRepository.save(user);
//           accountService.addRoleToUser("client1", "CLIENT");
//
//
//            Stream.of("employeur1").forEach(un -> {
//                accountService.saveEmployeur(1234,"safwen","kkkk","sousse",28467183,"saf@gmail.com","1234567","employeur1","1234567","pdg","800");
//            });
//
//            AppUser emp = userRepository.findByUsername("employeur1");
//            Role role1 = roleRepository.findByRoleName("EMPLOYEUR");
//            List<Role> roles1 = new ArrayList<>();
//            roles1.add(role1);
//            user.setRoleList(roles1);
//            userRepository.save(emp);
//            accountService.addRoleToUser("employeur1", "EMPLOYEUR");
//
//
//            Stream.of("admin1").forEach(un -> {
//                accountService.saveAdmin(1234,"safwen","kkkk","sousse",28467183,"saf@gmail.com","1234567","admin1","1234567");
//            });
//
//            AppUser admin = userRepository.findByUsername("admin1");
//            Role role2 = roleRepository.findByRoleName("ADMIN");
//            List<Role> roles2 = new ArrayList<>();
//            roles2.add(role2);
//            admin.setRoleList(roles2);
//            userRepository.save(admin);
//            accountService.addRoleToUser("admin1", "ADMIN");
//
//     };
//  }

}



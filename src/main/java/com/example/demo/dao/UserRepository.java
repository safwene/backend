package com.example.demo.dao;

import com.example.demo.model.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRepository extends MongoRepository<AppUser,String> {
@Query("{'id':?0}")
AppUser find_id(String id);
//@Query("{'login': ?0, 'motDePasse': ?1}")
//User login(String login, String motDePasse);
AppUser findByUsername(String username);
}

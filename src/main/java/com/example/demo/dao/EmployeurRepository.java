package com.example.demo.dao;

import com.example.demo.model.Employeur;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeurRepository extends CrudRepository<Employeur, String> {
    @Query("{'id': ?0}")
    Employeur find_id(String id);
}

package com.example.demo.dao;

import com.example.demo.model.SousCategorie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface SousCategorieRepository extends MongoRepository<SousCategorie, String> {
    @Query("{'id': ?0}")
    SousCategorie find_id(String id);
    @Query("{'categorie.id': ?0}")
    List<SousCategorie> find_categorie(String id);
}

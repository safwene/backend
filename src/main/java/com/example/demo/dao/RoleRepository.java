package com.example.demo.dao;

import com.example.demo.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface RoleRepository extends MongoRepository<Role,String> {
   Role findByRoleName(String roleName);
}

package com.example.demo.dao;

import com.example.demo.model.Facture;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface FactureRepository extends MongoRepository<Facture, String> {
    @Query("{'id': ?0}")
    Facture find_id(String id);
}

package com.example.demo.dao;

import com.example.demo.model.Client;
import org.aopalliance.reflect.Class;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, String> {
    @Query("{'id': ?0}")
    Client find_id(String id);
//    @Query("{'Class': ?0}")
//    Client find_class(String Class);
}

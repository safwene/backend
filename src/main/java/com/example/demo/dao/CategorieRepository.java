package com.example.demo.dao;

import com.example.demo.model.Categorie;
import com.example.demo.model.SousCategorie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface CategorieRepository extends MongoRepository<Categorie, String> {
    @Query("{'id': ?0}")
    Categorie find_id(String id);

    @Query("{'name': ?0}")
    Categorie find_name(String name);

    Categorie findByName(String name);

}

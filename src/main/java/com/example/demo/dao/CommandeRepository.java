package com.example.demo.dao;

import com.example.demo.model.Commande;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface CommandeRepository extends MongoRepository<Commande, String> {
    @Query("{'id': ?0}")
    Commande find_id(String id);
}

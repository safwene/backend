package com.example.demo.dao;

import com.example.demo.model.Produit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ProduitRepository extends MongoRepository<Produit, String> {
    @Query("{'id': ?0}")
    Produit find_id(String id);

//    Produit findByName(String name);
}

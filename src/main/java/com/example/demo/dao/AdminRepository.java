package com.example.demo.dao;

import com.example.demo.model.Admin;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<Admin, String> {
    @Query("{'id': ?0}")
    Admin find_id(String id);
}

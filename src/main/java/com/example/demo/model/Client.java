package com.example.demo.model;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document( collection = "utilisateurs")
@TypeAlias("Client")
public class Client extends AppUser{

    private String adresse;

    public Client() {
    }

    public Client(String id, int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword,
                  String username, String password, List<Role> roleList, String adresse) {
        super(id, cin, nom, prenom, ville, telephone, email, confirmedPassword, username, password, roleList);
        this.adresse = adresse;
    }

    public Client(String adresse) {
        this.adresse = adresse;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}

package com.example.demo.model;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document( collection = "utilisateurs")
@TypeAlias("Employeur")
public class Employeur extends AppUser{


    private String post;
    private String salaire;

    public Employeur() {
    }

    public Employeur(String id, int cin, String nom, String prenom, String ville, double telephone, String email, String confirmedPassword, String username,
                     String password, List<Role> roleList, String post, String salaire) {
        super(id, cin, nom, prenom, ville, telephone, email, confirmedPassword, username, password, roleList);
        this.post = post;
        this.salaire = salaire;
    }

    public Employeur(String post, String salaire) {
        this.post = post;
        this.salaire = salaire;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getSalaire() {
        return salaire;
    }

    public void setSalaire(String salaire) {
        this.salaire = salaire;
    }
}

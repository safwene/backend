package com.example.demo.model;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document (collection = "utilisateurs")
@TypeAlias("Admin")
public class Admin extends AppUser{

    public Admin() {
    }

    public Admin(String id, int cin, String nom, String prenom, String ville, double telephone, String email,
                 String confirmedPassword, String username, String password, List<Role> roleList) {
        super(id, cin, nom, prenom, ville, telephone, email, confirmedPassword, username, password, roleList);
    }


}

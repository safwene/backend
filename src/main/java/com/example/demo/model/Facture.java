package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class Facture {
    @Id
    private String id;
    private int num;
    private String date;
    private double prixTotal;
    @DBRef
    private List<Produit> produitList;
//    private String NomProduit;
//    private int QuantiteProduit;
//
//    private String NomClient;
//    private float price;
//    private float total=this.price*this.QuantiteProduit;

@DBRef
private Client client;

    public Facture() {
    }

    public Facture(String id, int num, String date, double prixTotal, List<Produit> produitList, Client client) {
        this.id = id;
        this.num = num;
        this.date = date;
        this.prixTotal = prixTotal;
        this.produitList = produitList;
        this.client = client;
    }

    public double getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(double prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}

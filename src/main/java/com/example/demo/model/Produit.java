package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Produit {
    @Id
    private String id;
    private String status;
    private String time;
    private String nom;
    private int quantite;
    private String photo;
    private float price;
    @DBRef
    private SousCategorie sousCategorie;

    public Produit() {
    }

    public Produit(String id, String status, String time, String nom, int quantite, String photo, float price, SousCategorie sousCategorie) {
        this.id = id;
        this.status = status;
        this.time = time;
        this.nom = nom;
        this.quantite = quantite;
        this.photo = photo;
        this.price = price;
        this.sousCategorie = sousCategorie;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public SousCategorie getSousCategorie() {
        return sousCategorie;
    }

    public void setSousCategorie(SousCategorie sousCategorie) {
        this.sousCategorie = sousCategorie;
    }
}

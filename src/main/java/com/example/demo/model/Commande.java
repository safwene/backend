package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

public class Commande {
    @Id
    private String id;
    private String date;
    @DBRef
    private List<Produit> produitList;
    @DBRef
    private Client client;

    public Commande() {
    }

    public Commande(String id, String date, List<Produit> produitList, Client client) {
        this.id = id;
        this.date = date;
        this.produitList = produitList;
        this.client = client;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
